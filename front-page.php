<?php

get_header();
get_template_part("template-parts/acceuil/part", "banner");
get_template_part("template-parts/acceuil/part", "title");
get_template_part("template-parts/acceuil/part", "prestige");
get_template_part("template-parts/acceuil/part", "selected");
get_template_part("template-parts/acceuil/part", "inspiration");
get_template_part("template-parts/acceuil/part", "space__creation");
get_sidebar();
get_footer();
