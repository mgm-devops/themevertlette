<?php
require_once('inc/supports.php');
require_once('inc/assets.php');
require_once('inc/menus.php');
require_once('inc/apparence.php');

/**
 * Load WooCommerce compatibility file.
 */
if (class_exists('WooCommerce')) {
    require get_template_directory() . '/inc/woocommerce.php';
}
