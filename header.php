<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _ThemeVertlette
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
</head>

<body>
    <header id="masthead" class="site-header">
        <section class="background-information-top">
            <section class="bloc-page flex-grid informationTop-content">
                <a class="informationTop-content--locate-store" href=""><i class="fas fa-map-marker-alt"></i>Trouver un magasin</a>
                <p class="informationTop-content--text">Livraison gratuite sur toutes les commandes de plus de 2500$!</p>
                <nav class="nav">
                    <!-- wp_nav_menu(
                        array(
                            'theme_location' => 'header',
                            'menu_id'        => 'primary-menu',
                        )
                    ); -->
                    <ul>
                        <li class="nav--item"><a href=""><i class="far fa-heart"></i></a></li>
                        <li class="nav--item"><a href=""><i class="far fa-user"></i></a></li>
                        <li class="nav--item"><a href=""><i class="fas fa-shopping-cart"></i></a></li>
                        <li class="nav--item"><a href="">FR</a></li>
                    </ul>
                </nav>
            </section>
        </section>
        <section class="bloc-page flex-grid sidebar-content">
            <a href="<?= home_url('/'); ?>" title="<?= __('Homepage', '_themevertlette') ?>">
                <img class="sidebar-content--logo" src="<?= get_theme_mod('logo') ?>" alt="logo">
            </a>
            <section>
                <section class="flex-grid">
                    <?php
                    wp_nav_menu([
                        'theme_location' => 'header',
                        'menu_id'        => 'primary-menu',
                        'menu_class'     => 'main-navigation'
                    ]);
                    ?>
                    <nav class="nav1">
                        <ul>
                            <li class="nav1--item"><a href=""><i class="far fa-heart"></i></a></li>
                            <li class="nav1--item"><a href=""><i class="far fa-user"></i></a></li>
                            <li class="nav1--item"><a href=""><i class="fas fa-shopping-cart"></i></a></li>
                            <li class="nav1--item"><a href="">FR</a></li>
                        </ul>
                    </nav>
                    <span><i class="fas fa-search"></i></span>

                    <!-- <?php get_search_form() ?> -->
                    <span class="hidden-fa-bars"><i class="fas fa-bars"></i></span>
                </section>
            </section>
        </section>
    </header><!-- #masthead -->
    <main id="primary" class="site-main">