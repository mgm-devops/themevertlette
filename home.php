<?php

get_header();
get_template_part("template-parts/news/part", "title");
get_template_part("template-parts/news/part", "card__news");
get_template_part("template-parts/news/part", "big__trend");
get_template_part("template-parts/news/part", "bright__color#1");
get_template_part("template-parts/news/part", "outside#2");
get_template_part("template-parts/news/part", "functionality#3");
get_template_part("template-parts/news/part", "return#4");
get_footer();
