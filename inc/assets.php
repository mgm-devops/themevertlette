<?php

/**
 * Enqueue scripts and styles.
 */
function _themevertlette_scripts()
{
    wp_enqueue_style('_themevertlette-style', get_stylesheet_uri(), array(), _S_VERSION);
    wp_enqueue_style('font-awesome', '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css');
    wp_style_add_data('_themevertlette-style', 'rtl', 'replace');

    wp_enqueue_script('_themevertlette-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', '_themevertlette_scripts');
