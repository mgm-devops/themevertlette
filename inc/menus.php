<?php

/**
 * Register widget area.
 */

function _themevertlette_widgets_init()
{
    register_sidebar([
        'id'            => 'footer',
        'name'          => esc_html__('Footer', '_themevertlette'),
        'description'   => esc_html__('Add widgets here.', '_themevertlette'),
        'before_widget' => '<section class="description-content--text">',
        'after_widget'  => '</section>',
        'before_title'  => '<h4 class="description__title">',
        'after_title'   => '</h4>',
    ]);
}
add_action('widgets_init', '_themevertlette_widgets_init');


/**
 * Register nav-menu.
 */

function _themevertlette_menu()
{
    register_nav_menus([
        'header' => esc_html__('Main navigation', '_themevertlette'),
    ]);
}
add_action('after_setup_theme', '_themevertlette_menu');
