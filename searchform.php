<form action="<?php esc_url(home_url('/')) ?>">
    <input name="s" type="search" placeholder="Recherche" aria-label="search" value="<?php get_search_query() ?>">
    <button type="submit">Rechercher</button>
</form>