<?php get_header(); ?>

<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <section class="flex-grid prestige-content padding__post">
            <section class="bloc-page">
                <h4 class="color__title"><?php the_title() ?></h4>
                <article class="prestige-content--post__type__description">
                    <p><?php the_content() ?></p>
                </article>
                <a href="<?= home_url('/'); ?>" class="back-button">Retour</a>
            </section>
        </section>
    <?php endwhile ?>
<?php endif; ?>

<?php get_footer(); ?>