<!-- Section  -->
<!-- <section class="flex-grid inspiration-content">
    <section class="inspiration-content--width50">
        <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/indoors-3101776_1920.jpg" alt="mobilier">
    </section>
    <section class="inspiration-content--width50">
        <section class="bloc-page">
            <h4>Inspiration</h4>
            <p>Laissez-vous inspirer par notre collection luxueuse.</p>
            <button class="inspiration-content--style-bottom">Voir nos inspirations</button>
        </section>
    </section>
</section> -->
<?php if (have_posts($post = 124)) : ?>
    <section class="flex-grid inspiration-content">
        <section class="inspiration-content--width50">
            <?php the_post_thumbnail() ?>
        </section>
        <section class="inspiration-content--width50">
            <section class="bloc-page">
                <article class="inspiration-content--flex__grid">
                    <h4><?php the_title() ?></h4>
                    <?php the_excerpt() ?>
                    <a href="<?php the_permalink() ?>" class="inspiration-content--style-bottom">Voir nos inspirations</a>
                </article>
            </section>
        </section>
    </section>
<?php endif; ?>