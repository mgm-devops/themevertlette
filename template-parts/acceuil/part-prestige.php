<!-- <section class="flex-grid prestige-content">
    <section class="prestige-content--width50">
        <section class="bloc-page">
            <h4>Prestige design</h4>
            <p>Raffinement, élégance et distinction sont les maîtres-mots qui définissent Luxembert. L’équipe créative,
                spécialisée dans la vente de mobilier, luminaires et accessoires décoratifs haut de gamme et dans la
                conception d’ambiances somptueuses, s’engage à servir une clientèle distinguée.</p>
            <button class="prestige-content--style-bottom">Lire la suite</button>
        </section>
    </section>
    <section class="prestige-content--width50">
        <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/hotel-1233020_1280.jpg" alt="hotel">
    </section>
</section> -->
<?php if (have_posts($post = 112)) : ?>
    <section class="flex-grid prestige-content">
        <section class="prestige-content--width50">
            <section class="bloc-page">
                <article class="prestige-content--flex__grid">
                    <h4><?php the_title() ?></h4>
                    <?php the_excerpt() ?>
                    <a href="<?php the_permalink() ?>" class="prestige-content--style-bottom">Lire la suite</a>
                </article>
            </section>
        </section>
        <section class="prestige-content--width50">
            <?php the_post_thumbnail() ?>
        </section>
    </section>
<?php endif; ?>