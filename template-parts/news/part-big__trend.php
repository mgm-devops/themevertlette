<!-- Section -->
<section class="bloc-page big-trend-card">
    <section class="big-trend-card--title">
        <h3>4 grande tendances meubles<br> et d’éco à adopter</h3>
        <p class="big-trend-card--color_text"><span>Source:</span> BrandSource</p>
    </section>
    <section class="flex-grid big-trend">
        <section class="big-trend--width50">
            <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/4-grandes-tendances-deco-en-2020-1024x683-1.jpg" alt="hotel">
        </section>
        <section class="big-trend--width50">
            <section class="bloc-page">
                <p>Le début d’une nouvelle année est un moment propice pour revitaliser son corps et rafraîchir le décor de sa maison. L'année 2020 nous apporte quelques
                    tendances inspirantes en matière de décoration intérieure, et contrairement à ce que laissait
                    entrevoir Retour vers le futur, elles mettent l’accent sur les couleurs vives, la nature, la fonctionnalité et un avenir durable pour notre planète.
                </p>
                <p>Nous avons réduit notre liste à quatre principales tendances en matière de décoration et d'aménagement intérieur en 2020 et nous vous proposons des
                    moyens faciles de les intégrer dans votre maison. À votre prochain souper d’amis, vos invités seront épatés par tous ces petits changements que vous
                    avez apportés dans votre intérieur.</p>
                <p>Sans plus tarder, voici les quatre tendances marquantes en décoration intérieure pour 2020 :</p>
            </section>
        </section>
    </section>
</section>