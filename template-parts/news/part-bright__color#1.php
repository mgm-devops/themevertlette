<!-- section -->
<section class="bloc-page big-trend-card">
    <section class="big-trend-card--title">
        <h3>1. Les couleurs vives</h3>
    </section>
    <section class="flex-grid big-trend">
        <section class="big-trend--width50">
            <section class="big-trend--bloc-page">
                <p>On oublie le blanc intégral dans la cuisine, car en 2020 (la tendance se dessinait depuis un certain temps) les couleurs éclatantes
                    sont très présentes. Pour un look classique, mais au goût du jour : les armoires du bas peintes en bleu classique, la couleur
                    de l’année Pantone ou un îlot de cuisine peint avec ce bleu ou toute autre couleur, dont le noir.</p>
                <p>L'utilisation audacieuse de la couleur apparaît également dans les salles d'eau et les buanderies entièrement décorées et affichant une abondance de
                    papiers peints floraux audacieux et colorés, d’œuvres d'art éclectiques et d’éclairage de luxe. Le style maximaliste rassemble des collections
                    hétéroclites,une utilisation audacieuse des couleurs et des motifs, et fait appel à des tissus, des textures et des couches multiples pour rassembler
                    le tout. Le style maximaliste vous permet de célébrer votre vie, vos voyages et votre bonheur par la décoration de votre intérieur.</p>
            </section>
        </section>
        <section class="big-trend--width50">
            <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/living-room-1157051_1280.jpg" alt="hotel">
        </section>
    </section>
</section>