<!-- section -->
<section class="bloc-page big-trend-card">
    <section class="big-trend-card--title">
        <h3>3. La fonctionnalité avant tout</h3>
    </section>
    <section class="flex-grid big-trend">
        <section class="big-trend--width50">
            <section class="big-trend--bloc-page">
                <p>Une grande tendance en 2020 est de penser à l’aspect fonctionnel pour que les objets soient utiles dans notre intérieur et dans l’environnement.
                    Avec le nombre accru de petites maisons et des espaces de vie plus petits (mais toujours élégants!), la demande de mobilier multifonctionnel
                    n'a jamais été aussi grande. Les designers réagissent à ce besoin en proposant des objets tels que des poufs avec du rangement, des rangements
                    sous le lit et des tables pouvant asseoir de deux à huit personnes.</p>
                <p>Mais il s'agit aussi de faire des choix fonctionnels pour l'environnement.
                    Faire des recherches, acheter des produits fabriqués plus près de chez soi, acheter des produits de qualité qui résisteront à l'épreuve du temps,
                    tout cela va faire de plus en plus partie de nos décisions en matière de décoration intérieure.</p>
            </section>
        </section>
        <section class="big-trend--width50">
            <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/Repose-pieds-avec-rangement-de-Signature-Design-1024x822-1.jpg" alt="hotel">
        </section>
    </section>
</section>