<!-- section -->
<section class="bloc-page big-trend-card">
    <section class="big-trend-card--title">
        <h3>2. L'extérieur à l'intérieur</h3>
    </section>
    <section class="flex-grid big-trend">
        <section class="big-trend--width50">
            <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/interior-design-4467770_1280.jpg" alt="hotel">
        </section>
        <section class="big-trend--width50">
            <section class="bloc-page line__text">
                <p>La notion de durabilité n'est pas nouvelle, mais en 2020, cette prise de position s'étendra encore plus au design d'intérieur.
                    Les plantes (les vraies, pas les fausses) continuent de gagner en popularité et nous voyons que les designers intègrent de manière
                    beaucoup plus ciblée des murs végétaux et des jardins intérieurs.</p>
                <p>La nature est également à la source d’un regain de popularité pour les imprimés et les motifs floraux. S'inspirant de l'époque victorienne,
                    la version moderne intègre partout de gros motifs de fleurs, notamment sur la literie, les murs recouverts de
                    papier peint ou de murales peintes à la main, les coussins décoratifs, les tapis et même avec des œuvres d’art abstraites évoquant les fleurs.</p>
                <p>Nous voyons également des choix de tissus naturels (par exemple des draps de lin) et l’utilisation de textiles, notamment le tissage de type
                    paille de Vienne dans des meubles comme les têtes de lit, les chaises de salle à manger et autres.
                    Les paniers naturels utilisés comme jardinières pour vos plantes d'intérieur sont le moyen idéal d'adopter cette tendance.</p>
            </section>
        </section>
    </section>
</section>