<!-- section -->
<section class="bloc-page big-trend-card">
    <section class="big-trend-card--title">
        <h3>4. Le retour du noir</h3>
    </section>
    <section class="flex-grid big-trend">
        <section class="big-trend--width50">
            <img src="http://vertlette.dev.local/wp-content/uploads/2020/09/render-1364441_1280.jpg" alt="hotel">
        </section>
        <section class="big-trend--width50">
            <section class="bloc-page line__text">
                <p>Le noir est de retour! Mais était-il vraiment parti? Ce printemps, on voit le noir revenir en force dans la décoration intérieure.
                    Le noir apparaît de plus en plus dans
                    la cuisine avec des accents métalliques noirs comme les robinets et les poignées des armoires de cuisine.</p>
                <p>Le noir fait son retour dans la peinture (pensez à des salles à manger entièrement noires avec des tables et des chaises de luxe en bois),
                    dans le mélange des métaux (très présent dans les cuisines et les salles de bain), et même dans les meubles noirs. Nous avons déjà mentionné les portes noires,
                    et nous ne pensons pas que cette tendance audacieuse s'arrêtera de sitôt. Vous voulez essayer d'incorporer un peu plus de
                    noir mat dans votre maison? Actualisez les poignées des tiroirs de cuisine avec une peinture en aérosol noir mat.</p>
            </section>
        </section>
    </section>
</section>